<b><i>Note to developers:</i></b><br>
To compile the project with a recent Xcode, the Applescript code that connects to the UI needs to be replaced with something contemporary. Xcode now removes the old AppleScript data from NIB files.
<hr>
<hr>
<h1>
	<img style="padding-bottom: 20px; padding-right: 20px; width: 128px; height: 128px; float: left;" alt="" src="Icon.png" />
	<br />
	Tune-O-Tag
</h1>
<h2>
	What Is It?
</h2>
Tune-O-Tag is a powerful text manipulation tool for the text tags of iTunes tracks. With a preview list of the changes (for safety), Tune-O-Tag makes complicated edits of large numbers of tracks easy.
<ul style="list-style-type: disc;">
	<li>Copy text between tags</li>
	<li>Insert text before, after, or in place of a tag text</li>
	<li>Do a search &amp; replace in the tag texts &#8211; literal, or regular expressions </li>
	<li>Set sequential numbers (arabic and roman) when a tag text changes from one track to the next (like different album, artist, or compilation, &#8230; )</li>
	<li>Write a marker text to the track in&nbsp;such a position in a list (useful as orientation when doing manual editing in iTunes)</li>
</ul>
<h2>
	How To Use and How It Works
</h2>
<ol>
	<li>Launch iTunes and select the tracks you want to edit, either as individual tracks or as the currently displayed playlist.</li>
	<li>In Tune-O-Tag, select your options:&nbsp;</li>
	<ul style="list-style-type: disc;">
		<li>work on selected tracks or on entire playlist</li>
		<li>the tag to read the text from</li>
		<li>what to do with the text</li>
		<li>the tag to write into</li>
		<li>where to place the replace result in the write-to tag.</li>
	</ul>
	<li>Enter the parameters for the action, e.g., search term and replace term, or start number and numbering step. </li>
	<li>Click "Build Preview", or hit enter or return. </li>
	<li>Tune-O-Tag then reads the text from each track, does its work on it, and combines the result with the existing text of the write-to tag.</li>
	<li>All three texts are displayed in a Preview window, so you can check if everything is how you want it (no files have been altered so far). </li>
	<li>If not, click "Cancel" or simply go back to the Tune-O-Tag main window to modify your editing. If everything is as desired click "Write to files", and the new tag texts are written to the track files. </li>
</ol>
<h2>
	Screenshots
</h2>
	<img style="padding-bottom: 20px; padding-right: 20px; float: left;" alt="" src="SCREENSHOTS/1. Track Selection.png" />
	<br>
	<img style="padding-bottom: 20px; padding-right: 20px; float: left;" alt="" src="SCREENSHOTS/2. Actions List.png" />
	<br>
	<img style="padding-bottom: 20px; padding-right: 20px; float: left;" alt="" src="SCREENSHOTS/3. Source Tags.png" />
	<br>
	<img style="padding-bottom: 20px; padding-right: 20px; float: left;" alt="" src="SCREENSHOTS/4. Target Placement.png" />
	<br>
	<img style="padding-bottom: 20px; padding-right: 20px; float: left;" alt="" src="SCREENSHOTS/5. Target Tags.png" />
	<br>
	<img style="padding-bottom: 20px; padding-right: 20px; float: left;" alt="" src="SCREENSHOTS/6. Example Regex-1.png" />
	<br>
	<img style="padding-bottom: 20px; padding-right: 20px; float: left;" alt="" src="SCREENSHOTS/7. Example Regex-2.png" />
<h2>
	The Text Manipulation Options
</h2>
<h3>
	Insert Text
</h3>
The entered text is written to all tracks.
<h3>
	Copy Unchanged
</h3>
Doesn't change the read text.
<h3>
	String Search/Replace
</h3>
All characters are searched for and replaced literally. For example, ".mp" will find&nbsp;and replace only ".mp".
<h3>
	Regular Expressions (Basic and Extended)
</h3>
Some characters have special meanings. One example: the dot stands for any character, so "..mp" will find "jump", "a.mp", "Vamp", "gimp", etc. See for example <a href="http://en.wikipedia.org/wiki/Regular_expression">the Wikipedia entry</a> for more info on regular expressions.<br />
The regular expressions are the same as in the BSD sed or vim in MacOS X. A brief reference can be shown/hidden with command-R or from the Help Menu. Use regular expressions only if you know what you are doing ("Some people, when confronted with a problem, think 'I know, I'll use regular expressions.' Now they have two problems." &#8211; Original author <a href="http://regex.info/blog/2006-09-15/247">unclear.</a>)
<h3>
	Number Sequentially
</h3>
Tune-O-Tag compares every read tag text with the text from the previous text. If they are identical, the outpout is the same number as for the previous track, if not, the count is increased. This is useful if you want to number tracks by your own criteria (by album, by artist, by grouping,&#8230;), e.g., to&nbsp;number&nbsp; the CDs of a compilation. You can set start number, step size, and output format (arabic or roman numerals; always enter start number and step size as arabic numbers).<br />
<ul>
	<li>Negative numbers a<span class="">re allowed.</span></li>
	<li class="">The tracks are read in the order of the iTunes display, from top to bottom of the list.</li>
	<li class="">Roman numerals are only legal for positive output numbers from 1 to 4999. Other roman output will return "***ERROR***".</li>
	<li><span class="">When reading from the <span style="font-style: italic;">Track Number</span> tag, not</span><span class="" style="font-style: italic;"> any different </span>number <span class="">causes a new sequential number, but </span><span class="" style="font-style: italic;">any number that is equal or less</span><span class=""> than the number from the previou</span>s track. This is useful to identify albums from their track numbers (a new album would usually begin with track 1).</li>
</ul>
<h3>
	Mark Every New
</h3>
Tune-O-Tag compares every read tag text with the text from the previous text. If they are identical, the outpout is empty, if not, Tune-O-Tag returns the entered marking text. (This is like Number Sequentially, but instead of the same number for <span style="font-style: italic;">every</span> track, the entered marking text is written <span style="font-style: italic;">only to the first </span>track where the tags were different (or, for read <span style="font-style: italic;">Track Numbers</span>, were <span style="font-style: italic;">less or equal</span>). Use this option to set a visual&nbsp;orientation for manual editing in iTunes, it works best with some strongly visible marking text, such as "&#63743;&#63743;&#63743;" in an otherwise unused tag.
<h2>
	Useful Tricks
</h2>
<h3>
	Backup
</h3>
Make a backup copy of your iTunes folder whenever you set out for some serious work with Tune-O-Tag. Even with the Preview window it is easy to overlook unwanted results from a complicated regular expression replace. Especially when you edit a large number of tracks &#8211; and that's where the damage would be the greatest.<br />
<h3>
	Playlist
</h3>
Make a new playlist with the tracks you want to edit. Running Tune-O-Tag on individually selected tracks takes about 40% longer, and one false click de-selects your carefully picked tracks.<br />
<h3>
	Test Runs
</h3>
Because there is no Cancel button, if you intend to work on a large playlist, test your edits on a few individually selected tracks first (without writing to files!). It will save some time.<br />
<h3>
	Temporary Tag
</h3>
Write your changes into a tag that is not used when optimizing a complicated search and replacement. This will leave the source tags intact until everything is perfect. Then you can copy your work to its final destination.<br />
You can also assemble a complicated new text in multiple steps from several source tags in a temporary tag.<br />
<h3>
	Delete All Text of Tags Containing
</h3>
Use regular expressions, search for text that apears only in the tag texts you want to delete, escape all special characters in it, put ".*" before and after it, and leave the replace term empty.<br />
<h3>
	Your Idea
</h3>
Post your tips in the place where you found Tune-O-Tag.
<h2>
	Requirements
</h2>
Mac OS 10.4. or 10.5 and iTunes (for early versions see Limitations &amp; Known Bugs). <br />
Tune-O-Tag was tested with Tiger and iTunes 4.7.1 and 8.0.2, and with Leopard and iTunes 7.5, 8.0.2 and 8.1 on PPC G4. <br />
If you have the developer tools installed (free download from Apple), and you know your ways, you can use the Xcode project included with the Tune-O-Tag application to build a Tune-O-Tag version that suits your needs.<br />
<h2>
	Limitations &amp; Known Bugs
</h2>
<ul style="list-style-type: disc;">
	<li>YOU MUST NOT WORK PARALLEL IN ITUNES AND TUNE-O-TAG. When you changed something in iTunes, an open Preview is obsolete and writing its content to files will yield unpredictable results at best.</li>
	<li>Tune-O-Tag was written in AppleScript: It works through commanding iTunes. And it is somewhat slow-ish&#8230;</li>
	<li>There is no Cancel button &#8211; once you run Build Preview or Write to Files Tune-O-Tag will continue its job until done, even when windows are closed. Hitting command-dot or escape in the Tune-O-Tag window will stop building a preview, but that's all.</li>
	<li>The Write to Files button is activated before building the Preview is complete. Wait until the table lines stop moving, or risk an error by clicking the write button too early.<br />
	</li>
	<li>Tune-O-Tag can only work on files that are stored on your computer or a plugged-in mobile device.</li>
	<li>Working on ringtones is not supported.</li>
	<li>Not all tags from the read tag list appear in the write-to tag list. Some tags are read-only for iTunes.</li>
	<li>Early iTunes versions do not support the full set of tags (at some time Apple increased their number from 32 to 59). If you work with such an early iTunes, tags that are not supported will throw an error and cause Tune-O-Tag to hang. Just quit and relaunch it, and chose another tag instead. (A list of supported tags can be opened in SkriptEditor typing command-shift-O and navigating to the desired iTunes version.)</li>
	<li>Searching for a backslash with regular expressions will return silly results if the read tag text contains multiple adjoining backslashes. Example: Searching in "1\2\\3\\\4\\\\5\\\\\6\\\\\\" for "\\" and replacing with "&#8226;" returns "1&#8226;2&#8226;3&#8226;&#8226;4&#8226;&#8226;5&#8226;&#8226;&#8226;6&#8226;&#8226;&#8226;". </li>
</ul>
<h2>
	Legal Stuff
</h2>
Copyright (c) 2009 Thorolf Wei&szlig;huhn &lt;therwe@web.de&gt;. Tune-O-Tag can be used and redistributed in source and binary forms, with or without modification, under certain conditions, see <a href="Tune-O-Tag%20License.html">license</a>. There is no warranty whatsoever, use at your own risk.
<h2>
	Credits
</h2>
The idea for a tool with a GUI with a preview came while playing with "iTunes SED" version 1.1 (2007) by Aurelio Jargas (and a modified earlier version of it (2005) by Bobby Holley) and "Search/Replace Tag Text" v1.0 (2005) by Doug Adams. <br />
The folks at macscripter.net were ready to help and explain where I was trying to do things the wrong way. A code snipped to convert arabic to roman numerals is courtesy of Piero Garzotto (http://pagina.de/piero). <br />
The pencil in the app's icon is based on a pencil by metalmarious and the disc is by Chrisdesign: both are public domain from openclipart.org. <br />
Thanks goes to all of them!
<h2>
	Version History
</h2>
v1.2.1 Mar 2009<br />
New text manipulation:&nbsp; Insert text.<br />
Minor Bugfix.<br />
<br />
&nbsp;v1.2 Mar 2009<br />
Now compatible with MacOS 10.4 (Tiger).<br />
New text manipulation:&nbsp;&nbsp;Copy text&nbsp;without modification.<br />
New text manipulation:&nbsp; Conditional sequential numbering.<br />
New text manipulation:&nbsp; Conditional marking.<br />
A&nbsp;host of new tags to read from or write to.<br />
Fixed a bug that caused Tune-O-Tag to hang if the Preview window was fiddled with while the preview was built or the files written.<br />
<br />
v1.1.1 Feb 2009<br />
Fixed a bug that threw an error when the search or replace terms for regular expressions contained a slash ("/") or backslash ("\"). <br />
Improved progress indiation when working on large numbers of individually selected tracks.<br />
Added Limitations &amp; Known Bugs section to Help/Readme text.<br />
<br />
v1.1 Feb 2009 <br />
Speed improvement: Tracks are not checked for being changeable if the selection or playlist has not changed.<br />
Improved progress indications.<br />
Improved Help/Readme text.<br />
<br />
v1.000001 Jan 2009 Added Readme and License to distributed archive.<br />
<br />
v1.0 Jan 2009 Going public.<br />
</div>
</body>
</html>
