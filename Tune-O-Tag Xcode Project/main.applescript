(* Copyright 2009 Thorolf E.R. Weißhuhn. All rights reserved, but see license in the help menu. *)

global OldTraklist
global traklist
global prevDataSource
global WriteTag
global NewWriteTextList
global writeLoc
global runBefore

on awake from nib theObject -- initialize
	if theObject's name is "MainWin" then
		set runBefore to false
		set OldTraklist to {}
	end if
end awake from nib


on choose menu item theObject -- here only interface changes
	if name of theObject = "MnLookup" then
		set visible of window "WndLookup" to not (visible of window "WndLookup")
	else if name of theObject is "PBtnSType" then -- interface changes depending on task
		tell window "MainWin"
			if title of popup button "PBtnSType" is "Insert Text" then -- interface for inserting (mostly the same as Mark Every New)
				set content of text field "MarkLbl" to "Insert" -- marking elements invisible, except label and text field
				set visible of text field "MarkLbl" to true
				set visible of text field "MarkFld" to true
				set visible of text field "MarkNote" to false
				set visible of text field "LblTrMark" to false
				set visible of popup button "PBtnreadtag" to false -- read tag elements invisible
				set visible of text field "LblRead" to false
				set visible of text field "StartLbl" to false -- counting elements invisible
				set visible of text field "StartFld" to false
				set visible of text field "StepLbl" to false
				set visible of text field "StepFld" to false
				set visible of text field "FormLbl" to false
				set visible of popup button "PBtnForm" to false
				set visible of text field "CountNote" to false
				set visible of text field "LblTrNum" to false
				set visible of text field "SrchLbl" to false -- S/R elements invisible
				set visible of text field "SrchFld" to false
				set visible of text field "RepLbl" to false
				set visible of text field "RepFld" to false
				set content of text field "ResultLbl" to "Write text" -- text as result 
			else if title of popup button "PBtnSType" is "Copy Unchanged" then -- interface for copying
				set visible of popup button "PBtnreadtag" to true -- read tag elements visible
				set visible of text field "LblRead" to true
				set visible of text field "MarkLbl" to false -- marking elements invisible
				set visible of text field "MarkFld" to false
				set visible of text field "MarkNote" to false
				set visible of text field "LblTrMark" to false
				set visible of text field "StartLbl" to false -- counting elements invisible
				set visible of text field "StartFld" to false
				set visible of text field "StepLbl" to false
				set visible of text field "StepFld" to false
				set visible of text field "FormLbl" to false
				set visible of popup button "PBtnForm" to false
				set visible of text field "CountNote" to false
				set visible of text field "LblTrNum" to false
				set visible of text field "SrchLbl" to false -- S/R elements invisible
				set visible of text field "SrchFld" to false
				set visible of text field "RepLbl" to false
				set visible of text field "RepFld" to false
				set content of text field "ResultLbl" to "Write this text" -- text as result
			else if title of popup button "PBtnSType" is "Number Sequentially" then -- interface for numbering
				set visible of popup button "PBtnreadtag" to true -- read tag elements visible
				set visible of text field "LblRead" to true
				set visible of text field "MarkLbl" to false -- marking elements invisible
				set visible of text field "MarkFld" to false
				set visible of text field "MarkNote" to false
				set visible of text field "LblTrMark" to false
				set visible of text field "StartLbl" to true -- counting elements visible
				set visible of text field "StartFld" to true
				set visible of text field "StepLbl" to true
				set visible of text field "StepFld" to true
				set visible of text field "FormLbl" to true
				set visible of popup button "PBtnForm" to true
				set visible of text field "CountNote" to true
				if title of popup button "PBtnreadtag" = "Track Number" then -- show/hide special note
					set visible of text field "LblTrNum" to true
				else
					set visible of text field "LblTrNum" to false
				end if
				set visible of text field "SrchLbl" to false -- S/R elements invisible
				set visible of text field "SrchFld" to false
				set visible of text field "RepLbl" to false
				set visible of text field "RepFld" to false
				set content of text field "ResultLbl" to "Write number" -- number as result 
			else if title of popup button "PBtnSType" is "Mark Every New" then -- interface for marking
				set visible of popup button "PBtnreadtag" to true -- read tag elements visible
				set visible of text field "LblRead" to true
				set content of text field "MarkLbl" to "Mark with" -- marking elements visible
				set visible of text field "MarkLbl" to true
				set visible of text field "MarkFld" to true
				set visible of text field "MarkNote" to true
				if title of popup button "PBtnreadtag" = "Track Number" then -- show/hide special note
					set visible of text field "LblTrMark" to true
				else
					set visible of text field "LblTrMark" to false
				end if
				set visible of text field "StartLbl" to false -- counting elements invisible
				set visible of text field "StartFld" to false
				set visible of text field "StepLbl" to false
				set visible of text field "StepFld" to false
				set visible of text field "FormLbl" to false
				set visible of popup button "PBtnForm" to false
				set visible of text field "CountNote" to false
				set visible of text field "LblTrNum" to false
				set visible of text field "SrchLbl" to false -- S/R elements invisible
				set visible of text field "SrchFld" to false
				set visible of text field "RepLbl" to false
				set visible of text field "RepFld" to false
				set content of text field "ResultLbl" to "Write mark" -- mark as result 
			else -- String Search/Replace, Basic Regular Expressions, Extended Regular Expressions -- interface for searching/replacing
				set visible of popup button "PBtnreadtag" to true -- read tag elements visible
				set visible of text field "LblRead" to true
				set visible of text field "MarkLbl" to false -- marking elements invisible
				set visible of text field "MarkFld" to false
				set visible of text field "MarkNote" to false
				set visible of text field "LblTrMark" to false
				set visible of text field "StartLbl" to false -- counting elements invisible
				set visible of text field "StartFld" to false
				set visible of text field "StepLbl" to false
				set visible of text field "StepFld" to false
				set visible of text field "FormLbl" to false
				set visible of popup button "PBtnForm" to false
				set visible of text field "CountNote" to false
				set visible of text field "LblTrNum" to false
				set visible of text field "SrchLbl" to true -- S/R elements visible
				set visible of text field "SrchFld" to true
				set visible of text field "RepLbl" to true
				set visible of text field "RepFld" to true
				set content of text field "ResultLbl" to "Write the result" -- replacement as result
			end if
		end tell
	else if name of theObject is "PBtnreadtag" then -- show/hide special note
		tell window "MainWin"
			if (title of popup button "PBtnreadtag" = "Track Number") and (title of popup button "PBtnSType" = "Number Sequentially") then
				set visible of text field "LblTrNum" to true
			else
				set visible of text field "LblTrNum" to false
			end if
			if (title of popup button "PBtnreadtag" = "Track Number") and (title of popup button "PBtnSType" = "Mark Every New") then
				set visible of text field "LblTrMark" to true
			else
				set visible of text field "LblTrMark" to false
			end if
		end tell
	end if
end choose menu item


on clicked theObject
	-- PREVIEW BUTTON
	if name of theObject = "BtnBuildPrev" then
		set visible of window "PrevWin" to false
		tell window "MainWin" -- UI while buildg preview
			set visible of button "BtnBuildPrev" to false
			set visible of text field "BuildgPrev" to true
			set visible of text field "GenrlNote" to false
			set indeterminate of progress indicator "PrevProgInd" to false
			set visible of progress indicator "PrevProgInd" to true
			set uses threaded animation of progress indicator "PrevProgInd" to true
		end tell
		try
			get MakePreview()
		on error makePrevErr
			tell window "PrevWin" to hide
			if makePrevErr is not "User canceled." then display dialog makePrevErr buttons {"OK"} default button 1 with icon stop
		end try
		tell window "MainWin" -- restore initial UI
			set visible of progress indicator "PrevProgInd" to false
			stop progress indicator "PrevProgInd"
			set visible of text field "BuildgPrev" to false
			set visible of text field "GenrlNote" to true
			set enabled of button "BtnBuildPrev" to true
			set visible of button "BtnBuildPrev" to true
		end tell
		
		-- APPLY BUTTON
	else if theObject's name = "BtnApply" then -- clicked apply
		set enabled of button "BtnBuildPrev" of window "MainWin" to false
		tell window of theObject
			set visible of button "BtnApply" to false
			set visible of button "BtnCancel" to false
			set visible of text field "Warning" to false
			set visible of text field "Applying" to true
			set indeterminate of progress indicator "ApplyProgInd" to false
			set visible of progress indicator "ApplyProgInd" to true
			set uses threaded animation of progress indicator "ApplyProgInd" to true
		end tell
		try
			get doApply()
		on error applyErr
			display dialog applyErr with icon stop
		end try
		tell window of theObject
			hide
			set visible of button "BtnApply" to true
			set visible of button "BtnCancel" to true
			set visible of text field "Warning" to true
			set visible of text field "Applying" to false
			set visible of progress indicator "ApplyProgInd" to false
			stop progress indicator "ApplyProgInd"
		end tell
		set enabled of button "BtnBuildPrev" of window "MainWin" to true
		
		-- CANCEL BUTTON
	else if name of theObject = "BtnCancel" then -- clicked cancel
		tell window of theObject
			hide
		end tell
	end if
end clicked


on MakePreview()
	-- FIRST PROGRESS INDICATION
	tell window "MainWin"
		set indeterminate of progress indicator "PrevProgInd" to true
		set content of text field "BuildgPrev" to "Gathering data …"
		start progress indicator "PrevProgInd"
		
		-- COLLECT PARAMETERS
		set TIDs to AppleScript's text item delimiters -- save the defaults
		set Traks to title of popup button "PBtnWhatTrks" -- => Displayed Playlist, Selected Tracks 
		set SType to title of popup button "PBtnSType" -- => Insert Text, Copy Unchanged, Number Sequentially, String Search/Replace, Basic Regular Expressions, Extended Regular Expressions
		set readtag to title of popup button "PBtnreadtag" as text
		set WriteTag to title of popup button "PBtnWriteTag" as text
		set writeLoc to title of popup button "PBtnLoc"
		set NumType to title of popup button "PBtnForm"
		if SType is "Insert Text" then -- get the text to insert (from same field as Mark Every New)
			if contents of text field "MarkFld" is not "" then -- not empty Mark field
				set InsertText to contents of text field "MarkFld"
			else
				stop progress indicator "PrevProgInd"
				display dialog "Nothing in text field." buttons {"OK"} default button 1 with icon "Search.tif"
				return
			end if
		else if SType is "Number Sequentially" then -- get data for numbering
			if (contents of text field "StepFld" = "") or (contents of text field "StartFld" = "") then -- empty fields
				stop progress indicator "PrevProgInd"
				display dialog "Required field is empty." buttons {"OK"} default button 1 with icon "Search.tif"
				return
			end if
			set StartWith to contents of text field "StartFld" as integer
			set incrementBy to contents of text field "StepFld" as integer
		else if SType is "Mark Every New" then -- get the marking text
			if contents of text field "MarkFld" is not "" then -- empty Mark field
				set TheMark to contents of text field "MarkFld"
			else
				stop progress indicator "PrevProgInd"
				display dialog "Mark field is empty." buttons {"OK"} default button 1 with icon "Search.tif"
				return
			end if
		else if SType is in {"String Search/Replace", "Basic Regular Expressions", "Extended Regular Expressions"} then -- get search/replace terms
			if contents of text field "SrchFld" is not "" then -- empty Search field
				set SStrg to contents of text field "SrchFld"
			else
				stop progress indicator "PrevProgInd"
				display dialog "Search field is empty." buttons {"OK"} default button 1 with icon "Search.tif"
				return
			end if
			set RStrg to contents of text field "RepFld"
		end if
	end tell
	
	-- GET VALID TRACKLIST FROM ITUNES 
	try
		if Traks = "Selected Tracks" then -- work on selected tracks
			tell application "iTunes" to set traklist to selection of front window
			if traklist is {} then -- needs at least one track
				display dialog "No tracks selected." buttons {"OK"} default button 1 with icon "Selection.tif"
				return
			end if
		else if Traks = "Displayed Playlist" then -- work on tracks of current playlist
			tell application "iTunes" to set PList to view of front window
			if PList is {} then -- one playlist required -- is this necessary at all? -- a safety valve anyway
				display dialog "No playlist selected." buttons {"OK"} default button 1 with icon "Playlist.tif"
				return
			end if
			tell application "iTunes" to set traklist to (every track of PList)
			if traklist is {} then -- needs at least one track
				display dialog "Empty playlist. Nothing to do." buttons {"OK"} default button 1 with icon "Nothing.tif"
				return
			end if
		end if
	on error
		display dialog "Error. iTunes does not support changing ringtones tags." buttons {"OK"} default button 1 with icon "Ringtones.tif"
		return
	end try
	
	-- CHECK TRACKS: CHANGEABLE?
	if traklist ≠ OldTraklist then -- check tracks only if new or changed traklist
		-- SECOND PROGRESS INDICATION
		set indeterminate of progress indicator "PrevProgInd" of window "MainWin" to false
		set maximum value of progress indicator "PrevProgInd" of window "MainWin" to (count traklist)
		start progress indicator "PrevProgInd" of window "MainWin"
		set content of text field "BuildgPrev" of window "MainWin" to "Checking Tracks …"
		
		set OKTraks to 0 -- a loop to count the changeable tracks; "get class of every item of" only works on a single object :-(
		repeat with M from 1 to (count items of traklist) -- process every track
			tell application "iTunes"
				try
					if contents of class of item M of traklist is in {file track, device track} then set OKTraks to OKTraks + 1
				end try
			end tell
			set the content of progress indicator "PrevProgInd" of window "MainWin" to M
		end repeat
		set diff to ((count traklist) - OKTraks) -- all tracks changeable?
		if diff = 1 then -- singular/plural for user feedback
			set dlgtext to "a track that is not a file"
		else if diff > 1 then
			set dlgtext to diff & " tracks that are not files"
		end if
		if diff > 0 then -- user feedback
			display dialog "The selection/playlist contains " & dlgtext & ¬
				" on your hard disk or portable player. They cannot be changed." buttons {"Cancel"} default button 1 with icon "NoChange.tif"
			return
		end if
	end if
	set OldTraklist to traklist -- store new list for next run
	
	-- THIRD PROGRESS INDICATION
	set indeterminate of progress indicator "PrevProgInd" of window "MainWin" to true
	start progress indicator "PrevProgInd" of window "MainWin"
	set content of text field "BuildgPrev" of window "MainWin" to "Reading tags …"
	
	-- READ TAGS FROM ITUNES / NAMES FROM FILES 
	-- read tags into separate lists in the same order => (traklist, readTextList, writeTextList)*(track1, track2, track3, ...)
	if Traks = "Selected Tracks" then
		set MadeLists to my makeTrakTagLists(readtag, WriteTag, traklist)
	else if Traks = "Displayed Playlist" then
		set MadeLists to my makePLTagLists(readtag, WriteTag, PList)
	end if
	set readTextList to item 1 of MadeLists
	set writeTextList to item 2 of MadeLists
	
	-- PREPARE TABLE VIEW
	if runBefore = false then -- data source for table view only if not existing
		set prevDataSource to make new data source at end of data sources with properties {name:"prevDataSource"}
		tell prevDataSource
			make new data column at the end of the data columns with properties {name:"ColOrigText"} -- <=> OldTextCol
			make new data column at the end of the data columns with properties {name:"ColChgdText"} -- <=> NewTextCol
			make new data column at the end of the data columns with properties {name:"ColWriteText"} -- <=> WriteTextCol
		end tell
		set the data source of table view "prevTView" of scroll view "prevSView" of window "PrevWin" to prevDataSource
		set runBefore to true
	else
		delete every data row of prevDataSource
	end if
	tell table view "prevTView" of scroll view "prevSView" of window "PrevWin"
		set contents of header cell of table column "OldTextCol" to "Original (" & readtag & ")"
		set contents of header cell of table column "NewTextCol" to "Result (" & SType & ")"
		set contents of header cell of table column "WriteTextCol" to "New text for " & WriteTag
	end tell
	
	-- PREPARE SHELL COMMAND
	if (SType is "Basic Regular Expressions") or (SType is "Extended Regular Expressions") then -- escape the separator
		if SStrg contains "/" then set SStrg to TextReplace(SStrg, "/", "\\/")
		if RStrg contains "/" then set RStrg to TextReplace(RStrg, "/", "\\/")
	end if
	if SType is "Basic Regular Expressions" then
		set ShCmd to (" | sed " & quoted form of ("s/" & SStrg & "/" & RStrg & "/g")) -- no -E option
	else if SType is "Extended Regular Expressions" then
		set ShCmd to (" | sed -E " & quoted form of ("s/" & SStrg & "/" & RStrg & "/g")) -- with -E
	end if
	
	-- FOURTH PROGRESS INDICATION
	set indeterminate of progress indicator "PrevProgInd" of window "MainWin" to false
	set maximum value of progress indicator "PrevProgInd" of window "MainWin" to (count traklist)
	start progress indicator "PrevProgInd" of window "MainWin"
	set content of text field "BuildgPrev" of window "MainWin" to "Building Preview …"
	
	-- PROHIBIT USER ACTION WHILE BUILDING
	tell scroll view "prevSView" of window "PrevWin"
		set has vertical scroller to false
		set has horizontal scroller to false
	end tell
	tell window "PrevWin" to set visible to true
	
	-- INITIALIZE FOR NUMBERING
	if (SType is "Number Sequentially") then -- prepare for numbering or marking
		set PrevText to (item 1 of readTextList) as Unicode text -- so that the first text isn't "different"
		set TheNumber to StartWith
	end if
	if SType is "Mark Every New" then set PrevText to (item 1 of readTextList) -- so that the first text isn't "different"
	
	-- PROCESS THE TAG TEXTS
	repeat with J from 1 to (count items of traklist) -- process every track
		set the content of progress indicator "PrevProgInd" of window "MainWin" to J
		set OriginalText to (item J of readTextList) as text -- COPYING
		if SType is "Copy Unchanged" then -- just copy it
			set ChangedText to OriginalText
			-- INSERTING
		else if SType is "Insert Text" then -- insert
			set ChangedText to InsertText
			-- MARKING
		else if SType is "Mark Every New" then -- marking
			if readtag = "Track Number" then -- track number
				if OriginalText as number ≤ PrevText as number then -- set mark if read track numbering begins again, i.e., with lower or equal number
					set ChangedText to TheMark
					set PrevText to OriginalText
				else
					set ChangedText to ""
					set PrevText to OriginalText
				end if
			else -- not track number
				if OriginalText ≠ PrevText then -- set mark if any kind of different
					set ChangedText to TheMark
					set PrevText to OriginalText
				else
					set ChangedText to ""
					set PrevText to OriginalText
				end if
			end if
			-- NUMBERING
		else if SType is "Number Sequentially" then -- numbering
			if readtag = "Track Number" then -- track number  
				if OriginalText as number ≤ PrevText as number then -- increment if read track numbering begins again, i.e., with lower or equal number
					set TheNumber to (TheNumber + incrementBy)
					set PrevText to OriginalText
				end if
			else -- not track number
				if OriginalText ≠ PrevText then -- increment if any kind of different
					set TheNumber to (TheNumber + incrementBy)
					set PrevText to OriginalText
				end if
			end if
			if NumType = "Arabic Numerals" then -- arabic numerals
				set ChangedText to TheNumber as text
			else if NumType = "Roman Numerals" then -- convert to roman numerals…
				if (TheNumber > 0) and (TheNumber ≤ 4999) then -- … if legal
					set ChangedText to RomConvert(TheNumber)
				else
					set ChangedText to "***ERROR***"
				end if
			end if
			-- SEARCHING/REPLACING
		else if SType is in {"String Search/Replace", "Basic Regular Expressions", "Extended Regular Expressions"} then -- searching/replacing
			if SType is "String Search/Replace" then -- search/replace using Text Item Delimiters
				considering case
					set AppleScript's text item delimiters to SStrg
					set tempText to text items of OriginalText
					set AppleScript's text item delimiters to RStrg
					set ChangedText to (tempText as text)
				end considering
			else -- search/replace using the shell with sed
				try
					set ChangedText to (do shell script ("echo " & quoted form of OriginalText & ShCmd))
				on error ShellErr
					display dialog ShellErr buttons {"OK"} default button 1 with icon "Shell.tif"
					return
				end try
			end if
		end if
		
		-- WRITE LOCATION & APPEND TO PREVIEW
		if writeLoc is "in front of" then -- insertion place for the changed text
			set NewWriteText to ChangedText & item J of writeTextList
		else if writeLoc is "after" then
			set NewWriteText to item J of writeTextList & ChangedText
		else if writeLoc is "replacing" then
			set NewWriteText to ChangedText
		end if
		append prevDataSource with {{OriginalText, ChangedText, NewWriteText}} -- new record in datasource 
		call method "scrollRowToVisible:" of (table view "prevTView" of scroll view "prevSView" of window "PrevWin") with parameter (J - 1) --show the new record
		set the content of progress indicator "PrevProgInd" of window "MainWin" to J
	end repeat
	
	set AppleScript's text item delimiters to TIDs -- back to defaults
	set NewWriteTextList to contents of data cell "ColWriteText" of data rows of prevDataSource -- list for writing to files
	
	-- ALLOW USER ACTION & RESET UI
	tell window "PrevWin"
		tell scroll view "prevSView"
			set has vertical scroller to true
			set has horizontal scroller to true
		end tell
		set visible of button "BtnApply" to true
		set visible of button "BtnCancel" to true
		set visible of text field "Warning" to true
	end tell
	call method "scrollRowToVisible:" of (table view "prevTView" of scroll view "prevSView" of window "PrevWin") with parameter (0) --show the top
end MakePreview


on TextReplace(Strg, OldPart, NewPart)
	set AppleScript's text item delimiters to OldPart
	set NewStrg to every text item of Strg
	set AppleScript's text item delimiters to NewPart
	return (NewStrg as Unicode text)
end TextReplace


on RomConvert(n) -- arabic to roman (max 3999) -- this code (modified) courtesy of Piero Garzotto (http://pagina.de/piero) 
	set r to ""
	repeat with i from 1 to (count (n as text))
		set r to item (((item -i of (n as text)) as integer) + 1) of item i of {{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, {"", "M", "MM", "MMM", "MMMM"}} & r
	end repeat
end RomConvert


on doApply() -- button apply in preview window clicked, here the real job
	-- WRITE TO ITUNES 
	tell window "PrevWin"
		set maximum value of progress indicator "ApplyProgInd" to (count traklist)
		start progress indicator "ApplyProgInd"
		tell scroll view "prevSView"
			set has vertical scroller to false
			set has horizontal scroller to false
		end tell
	end tell
	repeat with L from 1 to (count traklist)
		set the content of progress indicator "ApplyProgInd" of window "PrevWin" to L
		with transaction
			get my WriteTrakTag(WriteTag, item L of traklist, item L of NewWriteTextList)
			call method "scrollRowToVisible:" of (table view "prevTView" of scroll view "prevSView" of window "PrevWin") with parameter (0) --show the next record
			delete data row 1 of prevDataSource
		end transaction
	end repeat
	tell scroll view "prevSView" of window "PrevWin"
		set has vertical scroller to true
		set has horizontal scroller to true
	end tell
end doApply


on WriteTrakTag(WriteTag, thewritetrak, TheNewTag) -- WriteTrakTag(WriteTag, item L of traklist, item L of NewWriteTextList)
	tell application "iTunes"
		if WriteTag is "Track Name" then
			set the name of thewritetrak to TheNewTag
		else if WriteTag is "Album" then
			set the album of thewritetrak to TheNewTag
		else if WriteTag is "Album Artist" then
			set the album artist of thewritetrak to TheNewTag
		else if WriteTag is "Artist" then
			set the artist of thewritetrak to TheNewTag
		else if WriteTag is "Composer" then
			set the composer of thewritetrak to TheNewTag
		else if WriteTag is "Show" then
			set the show of thewritetrak to TheNewTag
		else if WriteTag is "Year" then
			set the year of thewritetrak to TheNewTag
		else if WriteTag is "Category" then
			set the category of thewritetrak to TheNewTag
		else if WriteTag is "Comment" then
			set the comment of thewritetrak to TheNewTag
		else if WriteTag is "Description" then
			set the description of thewritetrak to TheNewTag
		else if WriteTag is "Genre" then
			set the genre of thewritetrak to TheNewTag
		else if WriteTag is "Grouping" then
			set the grouping of thewritetrak to TheNewTag
		else if WriteTag is "Long Description" then
			set the long description of thewritetrak to TheNewTag
		else if WriteTag is "Lyrics" then
			set the lyrics of thewritetrak to TheNewTag
		else if WriteTag is "Track Number" then
			set the track number of thewritetrak to TheNewTag
		else if WriteTag is "Track Count" then
			set the track count of thewritetrak to TheNewTag
		else if WriteTag is "Disc Number" then
			set the disc number of thewritetrak to TheNewTag
		else if WriteTag is "Disc Count" then
			set the disc count of thewritetrak to TheNewTag
		else if WriteTag is "Episode Number" then
			set the episode number of thewritetrak to TheNewTag
		else if WriteTag is "Episode ID" then
			set the episode ID of thewritetrak to TheNewTag
		else if WriteTag is "Season Number" then
			set the season number of thewritetrak to TheNewTag
		else if WriteTag is "Rating" then
			set the rating of thewritetrak to TheNewTag
		else if WriteTag is "Album Rating" then
			set the album rating of thewritetrak to TheNewTag
		else if WriteTag is "Video Kind" then
			set the video kind of thewritetrak to TheNewTag
		else if WriteTag is "Sort Track Name" then
			set the sort name of thewritetrak to TheNewTag
		else if WriteTag is "Sort Album" then
			set the sort album of thewritetrak to TheNewTag
		else if WriteTag is "Sort Album Artist" then
			set the sort album artist of thewritetrak to TheNewTag
		else if WriteTag is "Sort Artist" then
			set the sort artist of thewritetrak to TheNewTag
		else if WriteTag is "Sort Composer" then
			set the sort composer of thewritetrak to TheNewTag
		else if WriteTag is "Sort Show" then
			set the sort show of thewritetrak to TheNewTag
		end if
	end tell
end WriteTrakTag


on makeTrakTagLists(readtag, WriteTag, traklist) -- makeTrakTagLists(readtag, WriteTag, traklist)
	tell application "iTunes"
		set tempReadTextList to {}
		set tempWriteTextList to {}
		repeat with theTrak in traklist
			if readtag is "File Name" then
				set AppleScript's text item delimiters to ":"
				set OnePath to location of theTrak
				set OneName to last string of (text items of (OnePath as text))
				set end of tempReadTextList to OneName
				set AppleScript's text item delimiters to ""
			else if readtag is "Track Name" then
				set end of tempReadTextList to name of theTrak
			else if readtag is "Album" then
				set end of tempReadTextList to album of theTrak
			else if readtag is "Album Artist" then
				set end of tempReadTextList to album artist of theTrak
			else if readtag is "Artist" then
				set end of tempReadTextList to artist of theTrak
			else if readtag is "Composer" then
				set end of tempReadTextList to composer of theTrak
			else if readtag is "Show" then
				set end of tempReadTextList to show of theTrak
			else if readtag is "Year" then
				set end of tempReadTextList to year of theTrak
			else if readtag is "Category" then
				set end of tempReadTextList to category of theTrak
			else if readtag is "Comment" then
				set end of tempReadTextList to comment of theTrak
			else if readtag is "Description" then
				set end of tempReadTextList to description of theTrak
			else if readtag is "Genre" then
				set end of tempReadTextList to genre of theTrak
			else if readtag is "Grouping" then
				set end of tempReadTextList to grouping of theTrak
			else if readtag is "Long Description" then
				set end of tempReadTextList to long description of theTrak
			else if readtag is "Lyrics" then
				set end of tempReadTextList to lyrics of theTrak
			else if readtag is "Track Number" then
				set end of tempReadTextList to track number of theTrak
			else if readtag is "Track Count" then
				set end of tempReadTextList to track count of theTrak
			else if readtag is "Disc Number" then
				set end of tempReadTextList to disc number of theTrak
			else if readtag is "Disc Count" then
				set end of tempReadTextList to disc count of theTrak
			else if readtag is "Episode Number" then
				set end of tempReadTextList to episode number of theTrak
			else if readtag is "Episode ID" then
				set end of tempReadTextList to episode ID of theTrak
			else if readtag is "Season Number" then
				set end of tempReadTextList to season number of theTrak
			else if readtag is "Rating" then
				set end of tempReadTextList to rating of theTrak
			else if readtag is "Album Rating" then
				set end of tempReadTextList to album rating of theTrak
			else if readtag is "Date Added" then
				set end of tempReadTextList to date added of theTrak
			else if readtag is "Modification Date" then
				set end of tempReadTextList to modification date of theTrak
			else if readtag is "Kind" then
				set end of tempReadTextList to kind of theTrak
			else if readtag is "Video Kind" then
				set end of tempReadTextList to video kind of theTrak
			else if readtag is "Sort Track Name" then
				set end of tempReadTextList to sort name of theTrak
			else if readtag is "Sort Album" then
				set end of tempReadTextList to sort album of theTrak
			else if readtag is "Sort Album Artist" then
				set end of tempReadTextList to sort album artist of theTrak
			else if readtag is "Sort Artist" then
				set end of tempReadTextList to sort artist of theTrak
			else if readtag is "Sort Composer" then
				set end of tempReadTextList to sort composer of theTrak
			else if readtag is "Sort Show" then
				set end of tempReadTextList to sort show of theTrak
			end if
			if WriteTag is "Track Name" then
				set end of tempWriteTextList to name of theTrak
			else if WriteTag is "Album" then
				set end of tempWriteTextList to album of theTrak
			else if WriteTag is "Album Artist" then
				set end of tempWriteTextList to album artist of theTrak
			else if WriteTag is "Artist" then
				set end of tempWriteTextList to artist of theTrak
			else if WriteTag is "Composer" then
				set end of tempWriteTextList to composer of theTrak
			else if WriteTag is "Show" then
				set end of tempWriteTextList to show of theTrak
			else if WriteTag is "Year" then
				set end of tempWriteTextList to year of theTrak
			else if WriteTag is "Category" then
				set end of tempWriteTextList to category of theTrak
			else if WriteTag is "Comment" then
				set end of tempWriteTextList to comment of theTrak
			else if WriteTag is "Description" then
				set end of tempWriteTextList to description of theTrak
			else if WriteTag is "Genre" then
				set end of tempWriteTextList to genre of theTrak
			else if WriteTag is "Grouping" then
				set end of tempWriteTextList to grouping of theTrak
			else if WriteTag is "Long Description" then
				set end of tempWriteTextList to long description of theTrak
			else if WriteTag is "Lyrics" then
				set end of tempWriteTextList to lyrics of theTrak
			else if WriteTag is "Track Number" then
				set end of tempWriteTextList to track number of theTrak
			else if WriteTag is "Track Count" then
				set end of tempWriteTextList to track count of theTrak
			else if WriteTag is "Disc Number" then
				set end of tempWriteTextList to disc number of theTrak
			else if WriteTag is "Disc Count" then
				set end of tempWriteTextList to disc count of theTrak
			else if WriteTag is "Episode Number" then
				set end of tempWriteTextList to episode number of theTrak
			else if WriteTag is "Episode ID" then
				set end of tempWriteTextList to episode ID of theTrak
			else if WriteTag is "Season Number" then
				set end of tempWriteTextList to season number of theTrak
			else if WriteTag is "Rating" then
				set end of tempWriteTextList to rating of theTrak
			else if WriteTag is "Album Rating" then
				set end of tempWriteTextList to album rating of theTrak
			else if WriteTag is "Date Added" then
				set end of tempWriteTextList to date added of theTrak
			else if WriteTag is "Modification Date" then
				set end of tempWriteTextList to modification date of theTrak
			else if WriteTag is "Kind" then
				set end of tempWriteTextList to kind of theTrak
			else if WriteTag is "Video Kind" then
				set end of tempWriteTextList to video kind of theTrak
			else if WriteTag is "Sort Track Name" then
				set end of tempWriteTextList to sort name of theTrak
			else if WriteTag is "Sort Album" then
				set end of tempWriteTextList to sort album of theTrak
			else if WriteTag is "Sort Album Artist" then
				set end of tempWriteTextList to sort album artist of theTrak
			else if WriteTag is "Sort Artist" then
				set end of tempWriteTextList to sort artist of theTrak
			else if WriteTag is "Sort Composer" then
				set end of tempWriteTextList to sort composer of theTrak
			else if WriteTag is "Sort Show" then
				set end of tempWriteTextList to sort show of theTrak
			end if
		end repeat
	end tell
	return {tempReadTextList, tempWriteTextList}
end makeTrakTagLists


on makePLTagLists(readtag, WriteTag, PList) -- makePLTagLists(readtag, WriteTag, PList)
	tell application "iTunes"
		tell PList -- PList = view of front window
			if readtag is "File Name" then
				set tempReadTextList to {}
				set AppleScript's text item delimiters to ":"
				set TrakPaths to location of every track
				repeat with OnePath in TrakPaths
					set OneName to last string of (text items of (OnePath as text))
					set end of tempReadTextList to OneName
				end repeat
				set AppleScript's text item delimiters to ""
			else if readtag is "Track Name" then
				set tempReadTextList to name of every track
			else if readtag is "Album" then
				set tempReadTextList to album of every track
			else if readtag is "Album Artist" then
				set tempReadTextList to album artist of every track
			else if readtag is "Artist" then
				set tempReadTextList to artist of every track
			else if readtag is "Composer" then
				set tempReadTextList to composer of every track
			else if readtag is "Show" then
				set tempReadTextList to show of every track
			else if readtag is "Year" then
				set tempReadTextList to year of every track
			else if readtag is "Category" then
				set tempReadTextList to category of every track
			else if readtag is "Comment" then
				set tempReadTextList to comment of every track
			else if readtag is "Description" then
				set tempReadTextList to description of every track
			else if readtag is "Genre" then
				set tempReadTextList to genre of every track
			else if readtag is "Grouping" then
				set tempReadTextList to grouping of every track
			else if readtag is "Long Description" then
				set tempReadTextList to long description of every track
			else if readtag is "Lyrics" then
				set tempReadTextList to lyrics of every track
			else if readtag is "Track Number" then
				set tempReadTextList to track number of every track
			else if readtag is "Track Count" then
				set tempReadTextList to track count of every track
			else if readtag is "Disc Number" then
				set tempReadTextList to disc number of every track
			else if readtag is "Disc Count" then
				set tempReadTextList to disc count of every track
			else if readtag is "Episode Number" then
				set tempReadTextList to episode number of every track
			else if readtag is "Episode ID" then
				set tempReadTextList to episode ID of every track
			else if readtag is "Season Number" then
				set tempReadTextList to season number of every track
			else if readtag is "Rating" then
				set tempReadTextList to rating of every track
			else if readtag is "Album Rating" then
				set tempReadTextList to album rating of every track
			else if readtag is "Date Added" then
				set tempReadTextList to date added of every track
			else if readtag is "Modification Date" then
				set tempReadTextList to modification date of every track
			else if readtag is "Kind" then
				set tempReadTextList to kind of every track
			else if readtag is "Video Kind" then
				set tempReadTextList to video kind of every track
			else if readtag is "Sort Track Name" then
				set tempReadTextList to sort name of every track
			else if readtag is "Sort Album" then
				set tempReadTextList to sort album of every track
			else if readtag is "Sort Album Artist" then
				set tempReadTextList to sort album artist of every track
			else if readtag is "Sort Artist" then
				set tempReadTextList to sort artist of every track
			else if readtag is "Sort Composer" then
				set tempReadTextList to sort composer of every track
			else if readtag is "Sort Show" then
				set tempReadTextList to sort show of every track
			end if
			if WriteTag is "Track Name" then -- write tags
				set tempWriteTextList to name of every track
			else if WriteTag is "Album" then
				set tempWriteTextList to album of every track
			else if WriteTag is "Album Artist" then
				set tempWriteTextList to album artist of every track
			else if WriteTag is "Artist" then
				set tempWriteTextList to artist of every track
			else if WriteTag is "Composer" then
				set tempWriteTextList to composer of every track
			else if WriteTag is "Show" then
				set tempWriteTextList to show of every track
			else if WriteTag is "Year" then
				set tempWriteTextList to year of every track
			else if WriteTag is "Category" then
				set tempWriteTextList to category of every track
			else if WriteTag is "Comment" then
				set tempWriteTextList to comment of every track
			else if WriteTag is "Description" then
				set tempWriteTextList to description of every track
			else if WriteTag is "Genre" then
				set tempWriteTextList to genre of every track
			else if WriteTag is "Grouping" then
				set tempWriteTextList to grouping of every track
			else if WriteTag is "Long Description" then
				set tempWriteTextList to long description of every track
			else if WriteTag is "Lyrics" then
				set tempWriteTextList to lyrics of every track
			else if WriteTag is "Track Number" then
				set tempWriteTextList to track number of every track
			else if WriteTag is "Track Count" then
				set tempWriteTextList to track count of every track
			else if WriteTag is "Disc Number" then
				set tempWriteTextList to disc number of every track
			else if WriteTag is "Disc Count" then
				set tempWriteTextList to disc count of every track
			else if WriteTag is "Episode Number" then
				set tempWriteTextList to episode number of every track
			else if WriteTag is "Episode ID" then
				set tempWriteTextList to episode ID of every track
			else if WriteTag is "Season Number" then
				set tempWriteTextList to season number of every track
			else if WriteTag is "Rating" then
				set tempWriteTextList to rating of every track
			else if WriteTag is "Album Rating" then
				set tempWriteTextList to album rating of every track
			else if WriteTag is "Date Added" then
				set tempWriteTextList to date added of every track
			else if WriteTag is "Modification Date" then
				set tempWriteTextList to modification date of every track
			else if WriteTag is "Kind" then
				set tempWriteTextList to kind of every track
			else if WriteTag is "Video Kind" then
				set tempWriteTextList to video kind of every track
			else if WriteTag is "Sort Track Name" then
				set tempWriteTextList to sort name of every track
			else if WriteTag is "Sort Album" then
				set tempWriteTextList to sort album of every track
			else if WriteTag is "Sort Album Artist" then
				set tempWriteTextList to sort album artist of every track
			else if WriteTag is "Sort Artist" then
				set tempWriteTextList to sort artist of every track
			else if WriteTag is "Sort Composer" then
				set tempWriteTextList to sort composer of every track
			else if WriteTag is "Sort Show" then
				set tempWriteTextList to sort show of every track
			end if
		end tell
	end tell
	return {tempReadTextList, tempWriteTextList}
end makePLTagLists
[aeiou]